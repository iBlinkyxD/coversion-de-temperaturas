def celsius_to_fahrenheit(celsius):
    return (celsius * 9/5) + 32

def fahrenheit_to_celsius(fahrenheit):
    return (fahrenheit - 32) * 5/9

def celsius_to_kelvin(celsius):
    return celsius + 273.15

def kelvin_to_celsius(kelvin):
    return kelvin - 273.15

def fahrenheit_to_kelvin(fahrenheit):
    celsius = fahrenheit_to_celsius(fahrenheit)
    return celsius_to_kelvin(celsius)

def kelvin_to_fahrenheit(kelvin):
    celsius = kelvin_to_celsius(kelvin)
    return celsius_to_fahrenheit(celsius)

if __name__ == "__main__":
    temperature_celsius = 25.0
    temperature_fahrenheit = celsius_to_fahrenheit(temperature_celsius)
    temperature_kelvin = celsius_to_kelvin(temperature_celsius)

    print(f"{temperature_celsius} degrees Celsius is equal to {temperature_fahrenheit:.2f} degrees Fahrenheit")
    print(f"{temperature_celsius} degrees Celsius is equal to {temperature_kelvin:.2f} Kelvin")