# Coversion de Temperaturas y Distancia

## Integrantes
- Kevin Joa - 1100305
- Harvis Encarnacion - 1092843
- Guillermo Gil - 1101156
- Yelimbert Javier - 1099454

## Definición general del software

El software, creado con el lenguaje python y de nombre **Conversion de Temperatura y Distancia**, consiste en un software conversor de temperatura y distancia.

### Este software consta de las siguientes funciones:

- Permite que el usuario seleccione una conversión de temperatura que desea.
- Permite que el usuario introduzca la temperatura seleccionada a convertir.
- Permite que el usuario seleccione una conversión de distancia que desea.
- Permite que el usuario introduzca la distancia seleccionada a convertir.

### Limitaciones:
- Tiene muy poca opciones de conversión de distancias.
- No tiene una función para volver a seleccionar una conversión diferente.