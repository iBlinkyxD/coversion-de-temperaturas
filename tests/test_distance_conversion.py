import unittest
from distance_conversion import meters_to_feet, feet_to_meters

class TestDistanceConversion(unittest.TestCase):
    def test_meters_to_feet(self):
        self.assertEqual(meters_to_feet(1), 3.28084)
        self.assertEqual(meters_to_feet(125), 410.105)
        self.assertEqual(meters_to_feet(50), 164.042)

    def test_feet_to_meters(self):
        self.assertEqual(feet_to_meters(3.28084), 1)
        self.assertEqual(feet_to_meters(25), 7.6199997561600075)
        self.assertEqual(feet_to_meters(50), 15.239999512320015)

if __name__ == "__main__":
    unittest.main(verbosity=2)