import unittest
from temperature_conversion import celsius_to_fahrenheit, fahrenheit_to_celsius, celsius_to_kelvin, kelvin_to_celsius, fahrenheit_to_kelvin, kelvin_to_fahrenheit

class TestTemperatureConversion(unittest.TestCase):
    def test_celsius_to_fahrenheit(self):
        self.assertEqual(celsius_to_fahrenheit(0), 32)
        self.assertEqual(celsius_to_fahrenheit(25), 77)
        self.assertEqual(celsius_to_fahrenheit(-10), 14)

    def test_fahrenheit_to_celcius(self):
        self.assertEqual(fahrenheit_to_celsius(32), 0)
        self.assertEqual(fahrenheit_to_celsius(77), 25)
        self.assertEqual(fahrenheit_to_celsius(-10), -23.333333333333332)
    
    def test_celsius_to_kelvin(self):
        self.assertEqual(celsius_to_kelvin(0),273.15)
        self.assertEqual(celsius_to_kelvin(25),298.15)
        self.assertEqual(celsius_to_kelvin(-10),263.15)

if __name__ == "__main__":
    unittest.main(verbosity=2)